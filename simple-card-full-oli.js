import {LitElement, html, css} from 'lit';

export class SimpleCardFullOli extends LitElement {
  static get styles() {
    return css`
      #contenedor {
        display: flex;
        justify-content: space-around;
        align-items: center;
        flex-direction: column;
        border: solid 1px gray;
        padding: 16px;
        max-width: 300px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        margin: 16px;
      }
    `;
  }

  static get properties() {
    return {
      title: {type: String},
      urlImage: {type: String},
      text: {type: String},
      close: {type: Boolean},
      buttonText: {type: String},
    };
  }

  constructor() {
    super();
    this.buttonText = 'cerrar';
  }

  render() {
    return html`
      <div id="contenedor">
        <div style="width: 100%">
          <div style="width: 100%; justify-content: center; display: flex;">
            <h4>${this.title}</h4>
          </div>
        </div>

        ${this.urlImage &&  html`
        <div style="width: 100%; justify-content: center; display: flex;">
          <img height="150" width="150" src=${this.urlImage} />
        </div>`} 
        ${this.text &&  html`
        <div style="width: 100%">
          <p>${this.text}</p>
        </div>
        `}
        ${this.close &&  html`
        <slot></slot>
        <div
          style="width: 100%; justify-content: space-around; display: flex;"
          id="closeId"
        >
          <button @click=${this._onClose}>${this.buttonText}</button>
        </div>
        `}
      </div>
    `;
  }

  disconnectedCallback() {
    console.log("bbbbuuuuuummm !!")
    //a realizar al final del ciclo de vida
  }


  _onClose() {
    this.remove()
  }
}

window.customElements.define('simple-card-full-oli', SimpleCardFullOli);
