# LitElement for Oliverio Petrecca

This project includes a sample component using LitElement with JavaScript.

## Setup

Install card:

```bash
npm install simple-card-full-oli
```

## Example

 <simple-card-full-oli
      urlImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFxO_A-tUkXIq98hIFilLjTOnx8AS_gSXPRcRk6kBozWfJirdF3iQxe6lxdTVdS-A5ask&usqp=CAU"
        title="Empleado"
        text="Soy otro texto para debajo de la imagen, puede cambiar con el anterior, o no, cada uno lo pone a su gusto"
></simple-card-full-oli>
